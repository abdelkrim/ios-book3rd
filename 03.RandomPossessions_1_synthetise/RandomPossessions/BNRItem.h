//
//  BNRItem.h
//  RandomPossessions
//
//  Created by Boujraf Abdelkrim on 22/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRItem : NSObject
{
  NSString *itemName;
  NSString *serialNumber;
  int valueInDollars;
  NSDate *dateCreated;
  
  BNRItem *containedItem;
  __weak BNRItem *container;
}


+ (id)randomItem;

- (id)initWithItemName: (NSString *)name
        valueInDollars:(int)value
          serialNumber:(NSString *)sNumber;

@property (nonatomic, readwrite, strong) NSString *itemName;
@property (nonatomic, readwrite, strong) NSString *serialNumber;
@property (nonatomic, readwrite) int valueInDollars;
@property (nonatomic, readonly, strong) NSDate *dateCreated;
@property (nonatomic, readwrite, strong) BNRItem *containedItem;
@property (nonatomic, readwrite, weak) BNRItem *container;

@end
