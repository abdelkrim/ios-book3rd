//
//  BNRItem.h
//  RandomPossessions
//
//  Created by Boujraf Abdelkrim on 22/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRItem : NSObject
{
  NSString *itemName;
  NSString *serialNumber;
  int valueInDollars;
  NSDate *dateCreated;
  
  BNRItem *containedItem;
  //v1 BNRItem *container;
  //v2 __weak BNRItem *container;
  //v3 it crashes the system (see fig 3.9) __unsafe_unretained BNRItem *container;
  __weak BNRItem *container;
}


+ (id)randomItem;

- (id)initWithItemName: (NSString *)name
        valueInDollars:(int)value
          serialNumber:(NSString *)sNumber;

//- (void)setItemName: (NSString *)str;
//- (NSString *)itemName;
@property (nonatomic, readwrite, strong) NSString *itemName;

//- (void)setSerialNumber: (NSString *)str;
//- (NSString *)serialNumber;
@property (nonatomic, readwrite, strong) NSString *serialNumber;

//- (void)setValueInDollars:(int)i;
//- (int)valueInDollars;
@property (nonatomic, readwrite) int valueInDollars;

//- (NSDate *) dateCreated;
@property (nonatomic, readonly, strong) NSDate *dateCreated;

//- (void)setContainedItem:(BNRItem *)i;
//- (BNRItem *) containedItem;
@property (nonatomic, readwrite, strong) BNRItem *containedItem;

//- (void)setContainer:(BNRItem *)i;
//- (BNRItem *)container;
@property (nonatomic, readwrite, weak) BNRItem *container;

@end
