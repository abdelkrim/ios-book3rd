//
//  BNRItem.m
//  RandomPossessions
//
//  Created by joeconway on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BNRItem.h"

@implementation BNRItem

+ (id)randomItem
{
  NSArray *randomAdjectiveList = [NSArray arrayWithObjects:@"Fluffy",
                                  @"Rusty",
                                  @"Shiny", nil];
  NSArray *randomNounList = [NSArray arrayWithObjects:@"Bear",
                             @"Spork",
                             @"Mac", nil];
  NSInteger adjectiveIndex = rand() % [randomAdjectiveList count];
  NSInteger nounIndex = rand() % [randomNounList count];
  
  NSString *randomName = [NSString stringWithFormat:@"%@ %@",
                          [randomAdjectiveList objectAtIndex:adjectiveIndex],
                          [randomNounList objectAtIndex:nounIndex]];
  int randomValue = rand() % 100;
  
  NSString *randomSerialNumber = [NSString stringWithFormat:@"%c%c%c%c%c",
                                  'O' + rand() % 10,
                                  'O' + rand() % 26,
                                  'O' + rand() % 10,
                                  'O' + rand() % 26,
                                  'O' + rand() % 10];
  
  BNRItem *newItem = 
  [[self alloc] initWithItemName:randomName valueInDollars:randomValue serialNumber:randomSerialNumber];
  
  return newItem;
  
}
- (id) init
{
  return [self initWithItemName:@"item" valueInDollars:0 serialNumber:@""];
}

- (id)initWithItemName: (NSString *)name
        valueInDollars:(int)value
          serialNumber:(NSString *)sNumber
{
  self = [super init];
  
  if (self) {
    [self setItemName:name];
    [self setValueInDollars:value];
    [self setSerialNumber:sNumber];
    dateCreated = [[NSDate alloc] init];
  }
  return self;
}

- (id)initWithItemName:(NSString *)name
{
  return [self initWithItemName:name valueInDollars:0 serialNumber:@""];
}

- (void) dealloc 
{
  NSLog(@"Destroyed: %@", self);
}
- (void)setItemName:(NSString *)str
{
  itemName = str;
}

- (NSString *)itemName
{
  return itemName;
}

- (void)setSerialNumber:(NSString *)str
{
  serialNumber = str;
}

- (NSString *)serialNumber
{
  return serialNumber;
}

- (void)setValueInDollars:(int)i
{
  valueInDollars = i;
}

- (int)valueInDollars
{
  return valueInDollars;
}

- (NSDate *)dateCreated
{
  return dateCreated;
}

- (NSString *) description
{
  NSString *descriptionString =
  [[NSString alloc] initWithFormat:@"%@ (%@): Worth $%d, recorded on %@", 
   itemName,
   serialNumber,
   valueInDollars,
   dateCreated];
  return descriptionString;
}
@end
