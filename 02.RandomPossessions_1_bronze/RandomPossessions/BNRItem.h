//
//  BNRItem.h
//  RandomPossessions
//
//  Created by Boujraf Abdelkrim on 22/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRItem : NSObject
{
  NSString *itemName;
  NSString *serialNumber;
  int valueInDollars;
  NSDate *dateCreated;
}

+ (id)randomItem;

- (id)initWithItemName: (NSString *)name
        valueInDollars:(int)value
          serialNumber:(NSString *)sNumber;

- (void)setItemName: (NSString *)str;
- (NSString *)itemName;

- (void)setSerialNumber: (NSString *)str;
- (NSString *)serialNumber;

- (void)setValueInDollars:(int)i;
- (int)valueInDollars;

- (NSDate *) dateCreated;

@end
