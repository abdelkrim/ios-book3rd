//
//  main.m
//  RandomPossessions
//
//  Created by Boujraf Abdelkrim on 22/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BNRItem.h"

int main(int argc, const char * argv[])
{

  @autoreleasepool {
    
    // create mutable array object, store its address in items variable
    //NSMutableArray *items = [[NSMutableArray alloc] init];
    
    BNRItem *backpack = [[BNRItem alloc] init];
    [backpack setItemName:@"Backpack"];
//    [items addObject:backpack];
    
    BNRItem *calculator = [[BNRItem alloc] init];
    [calculator setItemName:@"Calculator"];//
//    [items addObject:calculator];
    
    [backpack setContainedItem:calculator];
    
    backpack = nil;
    
    NSLog(@"Container: %@, ", [calculator container]);
    
    // destroy the array pointed to by items
    calculator = nil;
  }
    return 0;
}

