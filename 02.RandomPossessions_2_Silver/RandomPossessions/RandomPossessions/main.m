//
//  main.m
//  RandomPossessions
//
//  Created by Boujraf Abdelkrim on 22/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BNRItem.h"

int main(int argc, const char * argv[])
{

  @autoreleasepool {
      
    // create mutable array object, store its address in items variable
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    // send the mssage addObject: to the NSMutableArray pointed to
    // by the variable items, pasing a string each time
    [items addObject:@"One"];
    [items addObject:@"Two"];
    [items addObject:@"Three"];
    
    // send another message, insertObject;atIndex, to that same array object
    [items insertObject:@"Zero" atIndex:0];
    
    
    for (int i=0; i < 10; i++)
    {
      BNRItem *p = [BNRItem randomItem];

      [items addObject:p];
    }
    
    // for every item in the array as determined by sending count to items
    for (BNRItem *item in items) {
      // we get the ith object from the array and pass it as an argument to 
      // nslog which implicitly send the description message to that object
      
      NSLog(@"%@", item);
    }
    
//v1    BNRItem *p = [[BNRItem alloc] init];
//    [p setItemName:@"Red sofa"];
//    [p setSerialNumber:@"ABCDEF1@"];
//    [p setValueInDollars:100];
//v2    BNRItem *p = [[BNRItem alloc] initWithItemName:@"Red sofa" valueInDollars:100 serialNumber:@"serialNumberABCDEF1"];

    
    
//v1    NSLog(@"%@ %@ %@ %d", [p itemName], [p dateCreated], 
//          [p serialNumber], [p valueInDollars]);
//v2    NSLog(@"%@", p);

    // destroy the array pointed to by items
    items = nil;
  }
    return 0;
}

